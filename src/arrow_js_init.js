Deno.core.ops();

class ArrowRuntimeHelper {
    constructor() {
        this.argument = Deno.core.jsonOpSync('op_json_into', []);
    }
    getArgument() {
        return this.argument
    }
    response(data) {
        Deno.core.dispatchByName('op_json_out', Deno.core.encode(JSON.stringify(data)));
    }
}

const Arrow = new ArrowRuntimeHelper()
