#[macro_use]
use dotenv_codegen::dotenv;

use std::fs;
use std::path::Path;

pub fn setup() {
    let a = dotenv!("ARROWS_ROOT_DIR");
    let b = format!("{}{}", a, dotenv!("SUB_DB_DIR"));

    let dir_vec = vec![a, &b];

    for i in dir_vec {
        if !Path::new(i).is_dir() {
            fs::create_dir(i).expect(&format!(
                "Unable to create directory at {} - please check permissions.",
                i
            ));
        }
    }

    println!("Setup complete!");
}
