#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;

mod arrow;
mod config;
mod js_runtimes;
mod setup;

use rocket_contrib::json::JsonValue;

use std::env;
use std::str;

#[get("/")]
fn index() -> &'static str {
    "Hello, world!\n"
}

#[catch(404)]
fn not_found() -> JsonValue {
    json!({
        "status": "error",
        "reason": "not found",
    })
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() > 1 && args[1] == "setup" {
        setup::setup();
    } else if args.len() > 1 {
        // improve
        println!("Unrecognised argument.");
        std::process::exit(1);
    } else {
        rocket::ignite()
            .mount("/", routes![index, arrow::arrow, arrow::arrow_with_entry])
            .register(catchers![not_found])
            .launch();
    }
}
