use deno_core::json_op_sync;
use deno_core::resolve_url_or_path;
use deno_core::Op;
use deno_runtime::{permissions, worker};

use std::cell::RefCell;
use std::path::PathBuf;
use std::rc::Rc;
use std::str;
use std::sync::Arc;

use crate::config::Config;

pub fn create_worker(module_path: PathBuf, config: Config) -> worker::MainWorker {
    let main_module = resolve_url_or_path(&module_path.to_string_lossy()).unwrap();
    let permissions = permissions::Permissions::from_options(&permissions::PermissionsOptions {
        allow_net: match config.permissions.net {
            true => Some(vec![]),
            false => None,
        },
        ..Default::default()
    });

    let options = worker::WorkerOptions {
        apply_source_maps: false,
        user_agent: "x".to_string(),
        args: vec![],
        debug_flag: false,
        unstable: false,
        ca_data: None,
        seed: None,
        js_error_create_fn: None,
        create_web_worker_cb: Arc::new(|_| unreachable!()),
        attach_inspector: false,
        maybe_inspector_server: None,
        should_break_on_first_statement: false,
        module_loader: Rc::new(deno_core::FsModuleLoader),
        runtime_version: "x".to_string(),
        ts_version: "x".to_string(),
        no_color: true,
        get_error_class_fn: None,
        location: None,
    };

    let mut worker = worker::MainWorker::from_options(main_module, permissions, &options);

    worker.bootstrap(&options);

    worker
}

pub fn worker_with_json(
    module_path: PathBuf,
    config: Config,
    data_in: String,
    json_out_as_string: Rc<RefCell<Option<String>>>,
) -> worker::MainWorker {
    let mut worker = create_worker(module_path, config);

    // refactor following ops into separate functions
    worker.js_runtime.register_op(
        "op_json_into",
        json_op_sync(move |_state, _j: Vec<f64>, _zero_copy| Ok(data_in.clone())),
    );

    worker
        .js_runtime
        .register_op("op_json_out", move |_state, zero_copy| {
            for buf in zero_copy {
                match str::from_utf8(&buf) {
                    Ok(v) => *json_out_as_string.borrow_mut() = Some(v.to_owned()),
                    Err(_e) => (),
                };
            }

            Op::Sync(Box::new([]))
        });

    worker.execute(include_str!("./arrow_js_init.js")).unwrap();

    worker
}
