use crate::config;
use crate::js_runtimes;

use anyhow::anyhow;
use deno_core::error::AnyError;
use deno_core::resolve_url_or_path;
use deno_core::ModuleSpecifier;
use rocket::Data;
use rocket_contrib::json::JsonValue;
use serde_json::Value;

use std::cell::RefCell;
use std::path;
use std::path::PathBuf;
use std::rc::Rc;
use std::str;

macro_rules! unwrap_result_or_return {
    ( $e:expr ) => {
        match $e {
            Ok(x) => x,
	    Err(err) => return json!({"status": "error", "reason": err.to_string()})
        }
    }
}

fn arrow_setup(
    peek: &[u8],
    arrow_id: u8,
    arrow_entrypoint: Option<String>,
) -> (
    Result<&str, str::Utf8Error>,
    Rc<RefCell<Option<String>>>,
    Result<config::Config, AnyError>,
    Option<String>,
) {
    let data_in_as_str = str::from_utf8(peek);

    // rename?
    let runtime_output: Rc<RefCell<Option<String>>> = Rc::new(RefCell::new(None));

    // get config
    let config = config::read_config(path::Path::new(&format!("./js/{}/config.toml", arrow_id)));

    let module_to_load = match arrow_entrypoint {
        //removes extension - should be in own util function
        Some(val) => {
            let mut module_vec = val.split('.').collect::<Vec<&str>>();
            if module_vec.len() > 1 {
                module_vec.remove(module_vec.len() - 1);
                Some(module_vec.join("."))
            } else {
                Some(val)
            }
        }
        None => None,
    };

    (data_in_as_str, runtime_output, config, module_to_load)
}

fn load_and_run_module(
    module_path: PathBuf,
    module_specifier: ModuleSpecifier,
    config: config::Config,
    data_in_as_str: &str,
    ro_clone: Rc<RefCell<Option<String>>>,
) -> Result<(), AnyError> {
    let mut worker =
        js_runtimes::worker_with_json(module_path, config, data_in_as_str.to_string(), ro_clone);

    // run module in future
    let future = async move {
        let result = worker.execute_module(&module_specifier).await;
        if result.is_err() {
            return result;
        }
        worker.run_event_loop().await
    };

    let runtime_as_result = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build();

    let runtime = match runtime_as_result {
        Ok(val) => val,
        Err(err) => return Err(anyhow!(err)),
    };

    // required until rocket handles async
    match runtime.block_on(future) {
        Ok(_) => Ok(()),
        Err(err) => Err(err),
    }
}

fn arrow_response(runtime_output: Rc<RefCell<Option<String>>>) -> JsonValue {
    let result_as_string = match &*runtime_output.borrow() {
        Some(val) => val.to_owned(),
        None => "".to_string(),
    };

    match serde_json::from_str::<Value>(&result_as_string) {
        Ok(val) => {
            json!({
                "status": "success",
                "response": val
            })
        }
        Err(_) => {
            if result_as_string == *"" {
                json!({
                    "status": "success",
                })
            } else {
                json!({
                    "status": "success",
                    "response": result_as_string
                })
            }
        }
    }
}

#[post("/arrow/<arrow_id>/<arrow_entrypoint>", data = "<bytes_vec>")]
pub fn arrow_with_entry(bytes_vec: Data, arrow_id: u8, arrow_entrypoint: String) -> JsonValue {
    if bytes_vec.peek_complete() {
        let (data_in_result, runtime_output, config_result, module_to_load_option) =
            arrow_setup(&bytes_vec.peek(), arrow_id, Some(arrow_entrypoint));
        let ro_clone = runtime_output.clone();

        let data_in_as_str = unwrap_result_or_return!(data_in_result);
        let config = unwrap_result_or_return!(config_result);
        let module_to_load = match module_to_load_option {
            Some(val) => val,
            None => return json!({"status": "error", "reason": "entrypoint not provided"}),
        };
        // setup path to module from id
        let module_path = path::PathBuf::from(&format!("./js/{}/{}.js", arrow_id, module_to_load));
        let module_specifier =
            unwrap_result_or_return!(resolve_url_or_path(&module_path.to_string_lossy()));

        unwrap_result_or_return!(load_and_run_module(
            module_path,
            module_specifier,
            config,
            data_in_as_str,
            ro_clone
        ));

        arrow_response(runtime_output)
    } else {
        json!({"status": "error", "reason": "something got fucked up"})
    }
}

#[post("/arrow/<arrow_id>", data = "<bytes_vec>")]
pub fn arrow(bytes_vec: Data, arrow_id: u8) -> JsonValue {
    if bytes_vec.peek_complete() {
        let (data_in_result, runtime_output, config_result, module_to_load_option) =
            arrow_setup(&bytes_vec.peek(), arrow_id, None);
        let ro_clone = runtime_output.clone();

        let data_in_as_str = unwrap_result_or_return!(data_in_result);
        let config = unwrap_result_or_return!(config_result);
        let module_to_load = match module_to_load_option {
            Some(_) => {
                return json!({"status": "error", "reason": "entrypoint where there shouldn't be one"})
            }
            None => config.entrypoint.clone(),
        };

        // setup path to module from id
        let module_path = path::PathBuf::from(&format!("./js/{}/{}", arrow_id, module_to_load));
        let module_specifier =
            unwrap_result_or_return!(resolve_url_or_path(&module_path.to_string_lossy()));

        unwrap_result_or_return!(load_and_run_module(
            module_path,
            module_specifier,
            config,
            data_in_as_str,
            ro_clone
        ));

        arrow_response(runtime_output)
    } else {
        json!({"status": "error", "reason": "something got fucked up"})
    }
}
