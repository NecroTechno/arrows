use deno_core::error::AnyError;
use serde::Deserialize;
use std::fs::read_to_string;
use std::path;

#[derive(Deserialize)]
pub struct Config {
    pub entrypoint: String,
    pub permissions: Permissions,
}

#[derive(Deserialize)]
pub struct Permissions {
    pub net: bool,
}

pub fn read_config(path: &path::Path) -> Result<Config, AnyError> {
    let config_as_string = match read_to_string(path) {
        Ok(val) => val,
        Err(err) => return Err(AnyError::msg(err.to_string())),
    };

    match toml::from_str(&config_as_string) {
        Ok(config) => Ok(config),
        Err(err) => Err(AnyError::msg(err.to_string())),
    }
}
