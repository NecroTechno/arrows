import { sum } from './sum.js';

let json_in = JSON.parse(Arrow.getArgument());

let result = sum(json_in.a, json_in.b);

Arrow.response(result);
